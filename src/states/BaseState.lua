local BaseState = Class{}

function BaseState:init() end
function BaseState:enter() end
function BaseState:exit() end
function BaseState:update(dt) end
function BaseState:mousepressed(x, y, button) end
function BaseState:keypressed(key) end
function BaseState:draw() end

return BaseState
