local BASE = utils.getBase(...)
local BASEBASE = utils.getBase(BASE)

local BaseState = require(BASEBASE..".BaseState")
local InGameState = require(BASE..".InGameState")

local StartState = Class{__includes = BaseState}

function StartState:init()
end

function StartState:update(dt)
  gStateStack:push(InGameState())
end

function StartState:draw()
end

return StartState
