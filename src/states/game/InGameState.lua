local BASE = utils.getBase(...)
local BASEBASE = utils.getBase(BASE)

local BaseState = require(BASEBASE..".BaseState")
local IAttackState = require(BASE..".IAttackState")
local EnemyAttackState = require(BASE..".EnemyAttackState")
local IDefendState = require(BASE..".IDefendState")

local InGameState = Class{__includes = BaseState}

local Shop = require("src.Shop")
local Deck = require("src.Deck")

function InGameState:init()
  self.myShop = Shop(self, gWidth*0/10, 0, gWidth/10, gHeight)
  self.myDeck = Deck(self, gWidth/4, gHeight*4/5, gWidth/2, gHeight/5)
  self.enemyDeck = Deck(self, gWidth/4, 0, gWidth/2, gHeight/5)
  self.serverUrl = gServerUrl
  local max_id = nil
  local max_id_server_timestamp = nil
  local response = json_decode(http_request(self.serverUrl .. "/get_messages"))
  for _, message in ipairs(response.messages) do
    if message.type == "register_player" then
      max_id = message.sender
      max_id_server_timestamp = message.server_timestamp
    end
    self.lastHandledServerMessageId = message.server_message_id
  end
  self.lastServerConnectionTimestamp = response.server_timestamp
  self.enemyId = max_id
  self.myId = max_id ~= nil and max_id + 1 or 1
  if self.enemyId then
    self.enemyLastSeenTimestamp = max_id_server_timestamp
    print("Enemy found! (myId = "..self.myId..", enemyId = "..self.enemyId..")")
  end
  http_request(self.serverUrl .. "/post_message", json_encode({type = "register_player", sender = self.myId}))
  self.myEvents = {}
  self.enemyEvents = {}
end

function InGameState:findEnemy()
  local max_id = nil
  local max_id_server_timestamp = nil
  local response = json_decode(http_request(self.serverUrl .. "/get_messages"))
  for _, message in ipairs(response.messages) do
    if self.lastHandledServerMessageId == nil or message.server_message_id > self.lastHandledServerMessageId then
      if message.type == "register_player" then
        max_id = message.sender
        max_id_server_timestamp = message.server_timestamp
      end
      self.lastHandledServerMessageId = message.server_message_id
    end
  end
  self.lastServerConnectionTimestamp = response.server_timestamp
  if max_id ~= nil and max_id > self.myId then
    self.enemyId = max_id
    self.enemyLastSeenTimestamp = max_id_server_timestamp
    print("Enemy found! (myId = "..self.myId..", enemyId = "..self.enemyId..")")
  end
end

function InGameState:update(dt)
  if not self.enemyId then
    self:findEnemy()
  else
    if self.myId % 2  == 0 then
      gStateStack:push(IAttackState(self))
    else
      gStateStack:push(EnemyAttackState(self))
    end
  end
end

function InGameState:draw()
  love.graphics.setColor(1, 1, 1)
  love.graphics.draw(gImages.background, 0, 0, 0, gWidth/gImages.background:getWidth(), gHeight/gImages.background:getHeight())
  self.myShop:draw()
  self.myDeck:draw()
  self.enemyDeck:draw()
end

function InGameState:mousepressed(x, y, button)
end

function InGameState:addMyEvent(event_type, event_data)
  assert(event_type)
  table.insert(self.myEvents, {
    type = event_type,
    data = event_data
  })
end

function InGameState:updateMessages()
  assert(self.myId)
  assert(self.enemyId)
  for _, event in ipairs(self.myEvents) do
    if not event.sent then
      http_request(self.serverUrl .. "/post_message", json_encode({type = event.type, sender = self.myId, data = event.data}))
      event.sent = true
    end
  end
  local response = json_decode(http_request(self.serverUrl .. "/get_messages"))
  for _, event in ipairs(response.messages) do
    if event.server_message_id > self.lastHandledServerMessageId then
      if event.sender == self.enemyId then
        print(self.lastHandledServerMessageId, event.server_message_id)
        table.insert(self.enemyEvents, event)
        self.enemyLastSeenTimestamp = event.server_timestamp
      end
      self.lastHandledServerMessageId = event.server_message_id
    end
  end
end

return InGameState
