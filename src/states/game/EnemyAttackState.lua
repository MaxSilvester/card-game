local BASE = utils.getBase(...)
local BASEBASE = utils.getBase(BASE)

local BaseState = require(BASEBASE..".BaseState")
local Card = require("src.Card")

local EnemyAttackState = Class{__includes = BaseState}

function EnemyAttackState:init(inGameState)
  assert(inGameState)
  self.inGameState = inGameState
end

function EnemyAttackState:update(dt)
  self.inGameState:updateMessages()
  for _, event in ipairs(self.inGameState.enemyEvents) do
    if not event.handled then
      if event.type == "buyCard" then
        event.handled = true
        print("Adding card "..event.data.full_name.." to enemy deck!")
        self.inGameState.enemyDeck:addCard(setmetatable(deepcopy(event.data), Card))
      end
    end
  end
end

function EnemyAttackState:draw()
end

function EnemyAttackState:mousepressed(x, y, button)
  if self.inGameState.myShop:testPoint(x, y) and button ~= 1 then
    self.inGameState.myShop:mousepressed(x, y, button)
  elseif self.inGameState.enemyDeck:testPoint(x, y) then
    self.inGameState.enemyDeck:mousepressed(x, y, button)
  elseif self.inGameState.myDeck:testPoint(x, y) then
    self.inGameState.myDeck:mousepressed(x, y, button)
  end
end

function EnemyAttackState:buyCard(card)
  assert(card)
  self.inGameState:addMyEvent("buyCard", deepcopy(card))
  self.inGameState.myDeck:addCard(card)
end

return EnemyAttackState
