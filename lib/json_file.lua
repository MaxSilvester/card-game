--local BASE = string.reverse(string.gsub(string.reverse(...), "^[^.]+", ""))

--local json = require(BASE.."json")

local function decode_file(path)
  -- local f = io.open(path)
  -- local content = f:read("*all")
  -- f:close()
  local content, _ = love.filesystem.read(path)
  return json_decode(content)
end

return decode_file
